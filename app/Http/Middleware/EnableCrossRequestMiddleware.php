<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2021 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

/**
 * 跨域解决方案
 * @author 牧羊人
 * @since 2021/1/10
 * Class EnableCrossRequestMiddleware
 * @package App\Http\Middleware
 */
class EnableCrossRequestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        $origin = $request->server('HTTP_ORIGIN') ? $request->server('HTTP_ORIGIN') : '';
        $allow_origin = [
            'http://localhost:8080',
            'http://manage.evl.pro.rxthink.cn',
        ];
        if (in_array($origin, $allow_origin)) {
            //允许所有资源跨域
            $response->header('Access-Control-Allow-Origin', $origin);
            // 允许通过的响应报头
            $response->header('Access-Control-Allow-Headers', 'Origin, Content-Type, Cookie, X-CSRF-TOKEN, Accept, Authorization, X-XSRF-TOKEN');
            // 允许axios获取响应头中的Authorization
            $response->header('Access-Control-Expose-Headers', 'Authorization, authenticated');
            // 允许的请求方法
            $response->header('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, OPTIONS, DELETE');
            //允许的请求方法
            $response->header('Allow', 'GET, POST, PATCH, PUT, OPTIONS, delete');
            // 运行客户端携带证书式访问
            $response->header('Access-Control-Allow-Credentials', 'true');
        }
        return $response;
    }
}
