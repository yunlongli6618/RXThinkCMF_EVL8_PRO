<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2021 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace App\Services;

use App\Models\NoticeModel;

/**
 * 通知公告-服务类
 * @author 牧羊人
 * @since 2020/11/11
 * Class NoticeService
 * @package App\Services
 */
class NoticeService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2020/11/11
     * NoticeService constructor.
     */
    public function __construct()
    {
        $this->model = new NoticeModel();
    }

    /**
     * 设置置顶
     * @return array
     * @since 2020/11/21
     * @author 牧羊人
     */
    public function setIsTop()
    {
        $data = request()->all();
        if (!$data['id']) {
            return message('记录ID不能为空', false);
        }
        if (!$data['is_top']) {
            return message('设置置顶不能为空', false);
        }
        $error = '';
        $item = [
            'id' => $data['id'],
            'is_top' => $data['is_top']
        ];
        $rowId = $this->model->edit($item, $error);
        if (!$rowId) {
            return message($error, false);
        }
        return message();
    }

}
